package com.anlai.gateway.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

//自定义的全局过滤器
@Component
public class MyGlobalFilter implements GlobalFilter, Ordered {

    //注入Jackson工具类
    @Autowired
    private ObjectMapper objectMapper;

    @Override
    //过滤方法：业务增强
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        System.out.println("-----------------全局过滤器MyGlobalFilter---------------------");
        //目标：必须查询参数中有token，才能通过过滤器转发到具体微服务，否则，不转发（不允许访问）
        //获取请求和响应
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        //获取查询参数token
        String token = request.getQueryParams().getFirst("token");
        //判断token是否存在
        if(StringUtils.isBlank(token)){
            //没有token--没有权限

            //说明非法请求--没有认证，阻止转发
            //响应一个未授权的状态码
            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
            //设置写入响应的的内容
            String result="{\"flag\":false,\"code\":20003,\"message\":\"没有权限\"}";
            byte[] resultBytes = result.getBytes(StandardCharsets.UTF_8);
            /*String result = null;
            try {
                //对象转json
                result = objectMapper.writeValueAsString(new ResultDTO(false, ReturnCode.ACCESSERROR,"没有权限"));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            byte[] bytes = result.getBytes(StandardCharsets.UTF_8);*/

//            byte[] resultBytes = new byte[0];
//            try {
//                resultBytes = objectMapper.writeValueAsBytes(new ResultDTO(false, ReturnCode.ACCESSERROR, "没有权限"));
//            } catch (JsonProcessingException e) {
//                e.printStackTrace();
//            }

            //设置响应编码
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON_UTF8);
//            DataBuffer wrap = response.bufferFactory().wrap(bytes);
            DataBuffer wrap = response.bufferFactory().wrap(resultBytes);
            //直接设置响应结果,不向下走了
            return exchange.getResponse().writeWith(Flux.just(wrap));
            //直接返回(网关响应直接结束，不向下走了)
//            return exchange.getResponse().setComplete();
        }else{
            //有token
            //放行
            return chain.filter(exchange);
        }

//        return null;
    }

    //过滤器执行的顺序
    @Override
    public int getOrder() {
        //数字越小，越先执行
        return 0;
    }
}
