package com.anlai.gateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import java.util.Arrays;
import java.util.List;

//自定义的过滤器工厂
//注意：类名必须规则是：过滤器名字+GatewayFilterFactory
@Component
public class GetMyParamGatewayFilterFactory extends AbstractGatewayFilterFactory<GetMyParamGatewayFilterFactory.Config> {


    //过滤器具体功能的提供
    //参数：已经封装了配置文件中的参数的数据的对象
    @Override
    public GatewayFilter apply(Config config) {
//        return null;
//        return new GatewayFilter() {
//            @Override
//            public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//                return null;
//            }
//        };
        //返回网关过滤器的对象
        //参数1：交换机
        //参数2：过滤器链
        return (exchange, chain) -> {
            //需求：在控制台输出配置文件中指定名称的请求参数的值。
            //第一步：拿请求和响应对象
            ServerHttpRequest request = exchange.getRequest();
            ServerHttpResponse response = exchange.getResponse();
            //第二步：编写业务逻辑代码
            //获取所有的查询参数
            MultiValueMap<String, String> queryParams = request.getQueryParams();
            //判断一下，是否存在该查询参数
            if(queryParams.containsKey(config.queryParam)){
                //如果存在查询参数，获取指定的查询参数(xxx?username=1111)
                //只获取一个查询参数的值
                String paramValue = queryParams.getFirst(config.queryParam);
                System.out.println("1.获取了参数"+config.queryParam+"的值为："+paramValue);
                //获取该参数的所有值
//            List<String> strings = queryParams.get(config.queryParam);
            }else{
                System.out.println("没有获取了参数"+config.queryParam+"和值");
            }
//            return  null;
            //第三步：放行，继续调用下一个过滤器(责任链设计模式)
            return chain.filter(exchange);
        };
    }


    //默认构造器
    public GetMyParamGatewayFilterFactory() {
        //调用父类构造,封装参数
        super(Config.class);
    }

    //定义一个常量（节约资源），快捷键control+shift+u切换大小写
    public static final String QUERY_PARAM = "queryParam";

    @Override
    //封装配置参数的多个值到内部配置类中的字段的顺序（数组）
    public List<String> shortcutFieldOrder() {
        //数组转集合
//        return Arrays.asList("queryParam","queryParam2"....);
//        return Arrays.asList("queryParam");
        return Arrays.asList(QUERY_PARAM);
    }

    //内部静态类，用于封装yaml配置参数的值，比如- GetMyParam=username，拿username
    public static class Config {
        // 要声明多少个字段，跟要接收多少个值有关系
        private String queryParam;
        public String getQueryParam() {
            return queryParam;
        }
        public void setQueryParam(String queryParam) {
            this.queryParam = queryParam;
        }
    }
}
